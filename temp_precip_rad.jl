using NPZ
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates

data_path = "/Users/anand/Documents/data/forest-mortality/"

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 2:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.mk_12.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])

function get_polar_xy(y)
    n = size(y)[1]

    θ = [(360 / n) * (π / 180) * i for i = 1:n]
    x, y = get_xy_from_rθ(y, θ)

    push!(x, x[1])
    push!(y, y[1])

    return x, y
end

function get_xy_from_rθ(r, θ)
    x = r .* cos.(θ)
    y = r .* sin.(θ)
    return x, y
end




meanX = npzread(data_path * "mean_Xd.npy")
stdX = npzread(data_path * "std_Xd.npy")

meanX[:, 1] .= 1 / 100 .* meanX[:, 1]
meanX[:, 2] .= 10 .* meanX[:, 2]
meanX[:, 3] .= meanX[:, 3]

stdX[:, 1] .= (1 / 100) .* stdX[:, 1]
stdX[:, 2] .= (10) .* stdX[:, 2]
stdX[:, 3] .= stdX[:, 3]


x_mean_r, y_mean_r = get_polar_xy(meanX[:, 1])
x_mean_p, y_mean_p = get_polar_xy(meanX[:, 2])
x_mean_t, y_mean_t = get_polar_xy(meanX[:, 3])

x_std_p, y_std_p = get_polar_xy(stdX[:, 2])
x_lower_t, y_lower_t = get_polar_xy(meanX[:, 3] - stdX[:, 3])
x_upper_t, y_upper_t = get_polar_xy(meanX[:, 3] + stdX[:, 3])

function plot_errorbars(ax, meanX, stdX, color)
    x_lower_t, y_lower_t = get_polar_xy(meanX - stdX)
    x_upper_t, y_upper_t = get_polar_xy(meanX + stdX)


    for i = 1:12
        lines!(
            ax,
            [x_lower_t[i], x_upper_t[i]],
            [y_lower_t[i], y_upper_t[i]],
            color = color,
            linestyle = "--",
        )
    end


end

function plot_ref!(ax, y)
    y_ref = zeros(360) .+ y
    x, y = get_polar_xy(y_ref)
    lines!(ax, x, y, linestyle = :dash, color = "gray70")
end



function generate_month_ticks!(ax)
    r = 28

    for i = 1:12
        θ = 360 / 12 * π / 180 * i
        xlabel = Dates.monthname.(Int.(i))

        x2, y2 = get_xy_from_rθ(r, θ)
        lines!(ax, [0, x2], [0, y2], linestyle = :dash, color = "gray70")

        if 3π / 2 > θ > π / 2
            x2, y2 = get_xy_from_rθ(r + 3.5, θ)
        end

        text!(x2, y2, text = xlabel[1:3], rotation = atan(tan(θ)), color = "gray30")
        if i == 3
            ϵ = 2
            δ = π / 50
            text!((0 - ϵ / 2, 8), text = "8", color = "gray30")
            text!((0 - ϵ, 16), text = "16", color = "gray30")
            text!((0 - ϵ, 24), text = "24", color = "gray30")

        end
        xlims!(ax, (-35, 35))
        ylims!(ax, (-35, 35))

    end
end


fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)


f = Figure(resolution = (700, 700))
ax = Axis(f[2, 1])
hidespines!(ax)
hidedecorations!(ax)
lines!(ax, x_mean_r, y_mean_r, color = palette[12], label = "Radiation")
lines!(ax, x_mean_p, y_mean_p, color = palette[6], label = "Precipitation")
lines!(ax, x_mean_t, y_mean_t, color = palette[10], label = "Temperature")
f
plot_ref!(ax, 24)
plot_ref!(ax, 16)
plot_ref!(ax, 8)

generate_month_ticks!(ax)
f

Legend(f[1, 1], ax, orientation = :horizontal, framevisible = false)
f
save(
    "/Users/anand/Documents/data/forest-mortality/images/precip_rad_temp.png",
    f,
    px_per_unit = 5,
)
# plot_errorbars(ax, meanX[:,1], stdX[:,1], palette[12])
# plot_errorbars(ax, meanX[:,2], stdX[:,2], palette[6])
# plot_errorbars(ax, meanX[:,3], stdX[:,3], palette[10])

# f
