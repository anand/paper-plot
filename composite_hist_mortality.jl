using NPZ
using CairoMakie
using StatsBase
using ColorSchemes
using Dates

data_path = "/Users/anand/Documents/data/forest-mortality/"

x = npzread(data_path * "x_train.npy")
y = 100 * npzread(data_path * "y_train.npy") # Convert to percent
z = npzread(data_path * "z_train.npy")
r = npzread(data_path * "r_train.npy")
w_xd = npzread(data_path * "w_logisitic_xd.npy")
w_xd_xs = npzread(data_path * "w_logisitic_xd_xs.npy")[:, 1:108]


w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)


index = y .> percentile(y, 90)

comp = mean(x[index, :, :], dims = 1)
comp_std = std(x[index, :, :], dims = 1)

min_max = (-0.8, 0.8)

fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)

p_base = Dict(
    "p1" => "#29066B",
    "p2" => "#7D3AC1",
    "p3" => "#AF4BCE",
    "p4" => "#DB4CB2",
    "p5" => "#EB548C",
    "p6" => "#EA7369",
    "p7" => "#FOA584",
)

palette = ColorSchemes.tol_light.colors
palette = ColorSchemes.mk_12.colors

p = 90
z_bad = z[y.>percentile(y, p), :]
z_good = z[y.<=percentile(y, p), :]
y_bad = y[y.>percentile(y, p)]

function xtickformat(x)
    month_name = Dates.monthname.(Int.(x .% 12))

    for (i, m) in enumerate(x)
        year = Int(3 - floor(m / 12) - 1)

        if year == 0
            month_name[i] = month_name[i][1:3] * "_{t}"
        else
            month_name[i] = month_name[i][1:3] * "_{t$(-year)}"
        end
    end
    latexstring.(month_name)
end
# fig = Figure(; resolution=(400,400))
# ax = Axis3(fig[1,1])
# scatter!(ax, z_bad[:,8], z_bad[:,10], z_bad[:,11], color = y_bad,  colormap = :lajolla, markersize = 10)


# fig = Figure(; resolution=(800,800))
# ax = Axis3(fig[1,1])
# scatter!(ax, z[:,8], z[:,10], z[:,11], color = y,  colormap = :lajolla, markersize = 4)

# function LogRange(start, stop, length)
#     (2^(y) for y in range(log2(start) , stop = log2(stop), length=length))
# end
# bins = [ bin for bin in LogRange(minimum(y), maximum(y), 50)]
bins = [minimum(y):0.3:maximum(y);]

mean_r = mean(r, dims = 2)[:, 1, :, 1]

h1 = fit(Histogram, y, bins)
xmap1 = StatsBase.binindex.(Ref(h1), y)

xmap1

rad = zeros(size(bins)[1])
rad_std = zeros(size(bins)[1])
precip = zeros(size(bins)[1])
precip_std = zeros(size(bins)[1])
temp = zeros(size(bins)[1])
temp_std = zeros(size(bins)[1])



for i = 1:size(bins)[1]
    rad[i] = mean(mean_r[:, 1][xmap1.==i])
    rad_std[i] = std(mean_r[:, 1][xmap1.==i])
    precip[i] = mean(mean_r[:, 2][xmap1.==i])
    precip_std[i] = std(mean_r[:, 2][xmap1.==i])
    temp[i] = mean(mean_r[:, 3][xmap1.==i])
    temp_std[i] = std(mean_r[:, 3][xmap1.==i])
end

fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)

f = Figure(resolution = (1200, 500))
ax1 = Axis(
    f[2, 1],
    xgridvisible = false,
    ygridvisible = false,
    xlabel = "Biomass Loss (%)",
    ylabel = "Density Estimate",
)
ax2 = Axis(f[2, 1], yaxisposition = :right, xgridvisible = false, ygridvisible = false)
ax3 = Axis(
    f[2, 2],
    xticks = 2:6:36,
    xtickformat = xtickformat,
    ygridvisible = false,
    ylabel = "Standardized Climate [-]",
)

d = density!(ax1, y, colormap = :diverging_rainbow_bgymr_45_85_c67_n256, color = :x)
f
lines!(ax2, bins, rad, color = palette[12], linewidth = 2.0)
lines!(ax2, bins, precip, color = palette[6], linewidth = 2.0)
lines!(ax2, bins, temp, color = palette[10], linewidth = 2.0)
# band!(ax2, bins, rad-rad_std, rad+rad_std, color = (palette[4], 0.1))
# band!(ax2, bins, precip-precip_std, rad+rad_std, color = (palette[3], 0.1))
# band!(ax2, bins, temp-temp_std, rad+rad_std, color = (palette[8], 0.1))
f
hlines!(ax2, 0, color = :black, linestyle = "--")
vlines!(
    ax2,
    median(y),
    color = p_base["p6"],
    linestyle = "-.",
    label = "Median $(round(median(y),digits=2)) %",
)
vlines!(
    ax2,
    mean(y),
    color = p_base["p4"],
    linestyle = "--",
    label = "Mean $(round(mean(y),digits=2)) %",
)
vlines!(
    ax2,
    percentile(y, 90),
    color = p_base["p2"],
    linestyle = "--",
    label = "90th percentiele : $(round(percentile(y, 90),digits=2)) %",
)
xlims!(ax1, minimum(y), maximum(y))
xlims!(ax2, minimum(y), maximum(y))

ylims!(ax1, 0, 2)
xlims!(ax1, minimum(y), 8)

xlims!(ax2, minimum(y), 8)
axislegend(ax2, position = :ct)

f

l1 = lines!(ax3, comp[1, :, 1], color = palette[12])
l2 = lines!(ax3, comp[1, :, 2], color = palette[6])
l3 = lines!(ax3, comp[1, :, 3], color = palette[10])

s1 = scatter!(
    ax3,
    [1:36;],
    comp[1, :, 1],
    color = w_xd[:, 1],
    marker = :rect,
    colormap = :diverging_linear_bjr_30_55_c53_n256,
    colorrange = min_max,
    label = "Coefficients",
    markersize = 15,
)
s2 = scatter!(
    ax3,
    [1:36;],
    comp[1, :, 2],
    color = w_xd[:, 2],
    marker = :rect,
    colormap = :diverging_linear_bjr_30_55_c53_n256,
    colorrange = min_max,
    markersize = 15,
)
s3 = scatter!(
    ax3,
    [1:36;],
    comp[1, :, 3],
    color = w_xd[:, 3],
    marker = :rect,
    colormap = :diverging_linear_bjr_30_55_c53_n256,
    colorrange = min_max,
    markersize = 15,
)
hlines!(ax3, 0, color = :black, linestyle = "--")
f
Legend(
    f[1, 1:2],
    [l1, l2, l3],
    ["Radiation", "Precipitation", "Temperature"],
    orientation = :horizontal,
)

xlims!(ax3, (0.5, 36.5))
f
axislegend(position = :ct)
f
Colorbar(f[2, 3], s1, ticks = [-0.8:0.8:0.8;])
f
