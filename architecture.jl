using ColorSchemes
using Luxor

using Revise
using LuxNet
using MathTeXEngine

## Some color palette 

# p_base = Dict(
#     1 => "#29066B", 
#     2 => "#7D3AC1",
#     3 => "#AF4BCE",
#     4 => "#DB4CB2",
#     5 => "#EB548C",
#     6 => "#EA7369",
#     7 => "#F0A58F",
#     8 => "#FCEAE6",

#     )

# cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i in 2:8])

palette = ColorSchemes.mk_12.colors

@png begin
    text_label = Label(str = "XYZ", display = true)
    x_base = 0
    y = 0

    layer_1 = Tensor3D(position = Point(-100,0), n_stack = 4, x_offset_factor=-0.5, y_offset_factor= 0.5, text_label = text_label)
    # layer_1 = Tensor2D(position = Point(0,0), text_label = text_label, n_element_h=6)

    drawnet(layer_1)
    # tensor2D_2 = Tensor2D(n_element_h = 5, n_element_v = 36)
    layer_2 = Tensor3D(position = Point(100,0), n_stack = 4, x_offset_factor=-0.5, y_offset_factor= 1, text_label = text_label)

    print(rightmid(layer_1))
    link_1 = HorizontalLink(start=rightmid(layer_1), finish = leftmid(layer_2), color = palette[1])
    drawnet(link_1)
    drawnet(layer_2)
    # drawnet(layer_1)
    # drawnet(layer_2)
    arrow(Point(-100, 0), Point(100, 0))
    arrow(Point(0, -100), Point(0, 100))
    # arrow(Point(-30, -100), Point(30, -100))
    # arrow(Point(-30, 100), Point(30, 100))
    # arrow(Point(-30, 80), Point(-30, -80))
    # arrow(Point(30, 80), Point(30, -80))

    # drawnet([tensor2D_1, tensor2D_2])

end


@pdf begin
    setline(1)
    fontsize(14)
    factor = 16
    # arrow(Point(-680, 0), Point(680, 0))

    x_base = -650
    alpha = 0.9
    y = 0

    color_weather = reshape(col_arr(36, add_alpha(palette[12], alpha), add_alpha(palette[6], alpha), add_alpha(palette[10], alpha)), (36, 3))

    
    weather = Tensor2D(
        position = Point(x_base, y),
        h_scale = 0.5,
        n_element_h = 3,
        n_element_v = 36,
        color = color_weather,
    )
    drawnet(weather)

    text_label = Label(str = "36 X 3 X 256", display = true)

    layer_1 = Tensor3D(
                n_element_h = 3, 
                n_element_v = 36, 
                position = Point(x_base + 80, y), 
                h_scale = 0.5, 
                n_stack = 256//factor, 
                x_offset_factor=-factor*0.01, 
                y_offset_factor= factor*0.01, 
                text_label = text_label)
    
    drawnet(layer_1)

    drawnet(HorizontalLink(start=rightmid(weather), finish = leftmid(layer_1), color = palette[1]))
    
    text_label = Label(str = "18 X 3 X 256", display = true)

    layer_2 = Tensor3D(n_element_h = 3, 
                n_element_v = 18, 
                position = Point(x_base + 170, y), 
                h_scale = 0.5, 
                n_stack = 256//factor, 
                x_offset_factor=factor*-0.01, 
                y_offset_factor= factor*0.01, 
                text_label = text_label)
    drawnet(layer_2)

    drawnet(HorizontalLink(start = rightmid(layer_1), finish = leftmid(layer_2), color = palette[2]))
    
    text_label = Label(str = "18 X 3 X 512", display = true)

    layer_3 = Tensor3D(
        position = Point(x_base + 290, y),
        h_scale = 0.5,
        n_element_h = 3, 
        n_element_v = 18, 
        n_stack = 512//factor, 
        x_offset_factor=factor*-0.01, 
        y_offset_factor= factor*0.01, 
        text_label = text_label)
    drawnet(layer_3)
    drawnet(HorizontalLink(start = rightmid(layer_2), finish = leftmid(layer_3), color = palette[1]))

    text_label = Label(str = "9 X 3 X 512", display = true)

    layer_4 = Tensor3D(
        position = Point(x_base + 400, y), h_scale = 0.5,
        n_element_h = 3, n_element_v = 9,
        n_stack = 512//factor, x_offset_factor=factor*-0.01, y_offset_factor= factor*0.01, text_label = text_label)
    drawnet(layer_4)

    drawnet(HorizontalLink(start = rightmid(layer_3), finish = leftmid(layer_4), color = palette[2]))

    text_label = Label(str = "13824 X 1", display = true)

    
    layer_5 = Tensor2D(position = Point(x_base + 470, y), h_scale = 0.0035*factor,
                        n_element_h = 1, n_element_v = 13824//factor, text_label=text_label)
    drawnet(layer_5)
    
    drawnet(HorizontalLink(start = rightmid(layer_4), finish = leftmid(layer_5), color = palette[3]))

    layer_61 = Tensor2D(position = Point(x_base + 520, (-32 * 5) / 2 - 30), h_scale = 0.5, n_element_h = 1, n_element_v = 32)
    drawnet(layer_61)

    drawnet(HorizontalLink(start = rightmid(layer_5), finish = leftmid(layer_61)))

    layer_62 = Tensor2D(position = Point(x_base + 520, (32 * 5) / 2 + 30), h_scale = 0.5, n_element_h = 1, n_element_v = 32)
    drawnet(layer_62)

    drawnet(HorizontalLink(start = rightmid(layer_5), finish = leftmid(layer_62)))

    text(L"\mu", Point(-110,110), angle = - pi/2)
    text(L"\sigma", Point(-110,-110), angle = - pi/2)

    text(L"N(\mu, \sigma^2)", Point(-80,20), angle = - pi/2)

    alpha = 0.7

    color_list_all = col_arr(
        8,
        add_alpha(palette[12], alpha),
        add_alpha(palette[6], alpha),
        add_alpha(palette[10], alpha),
        add_alpha(palette[2], alpha),
    )

    layer_7 = Tensor2D(
        n_element_h = 1,
        n_element_v = 32,
        color = color_list_all,
    position = Point(x_base + 590, y), h_scale = 0.5
    )

    drawnet(layer_7)
    setdash("solid")


    decoder_base = 0

    color_latent_rad =
        col_arr(8, add_alpha(palette[12], alpha), add_alpha(palette[2], alpha))

  
    layer_81 = Tensor2D(
        position = Point(decoder_base, -150), h_scale = 0.5,
        n_element_h = 1,
        n_element_v = 16,

        color = color_latent_rad,
    )
    
    drawnet(layer_81)
    drawnet(HorizontalLink(start = rightmid(layer_7), finish = leftmid(layer_81)))
    setdash("solid")

    color_latent_precip =
        col_arr(8, add_alpha(palette[6], alpha), add_alpha(palette[2], alpha))

    # element = Element()
    layer_82 = Tensor2D(
        position = Point(decoder_base, y), h_scale = 0.5,
        n_element_h = 1,
        n_element_v = 16,
        color = color_latent_precip,
    )

    drawnet(layer_82)

    setdash("longdashed")
    drawnet(HorizontalLink(start = rightmid(layer_7), finish = leftmid(layer_82)))
    setdash("solid")

    color_latent_temp =
        col_arr(8, add_alpha(palette[10], alpha), add_alpha(palette[2], alpha))

    layer_83 = Tensor2D(
        position = Point(decoder_base, 150), h_scale = 0.5,
        n_element_h = 1,
        n_element_v = 16,
        color = color_latent_temp,
    )

    drawnet(layer_83)
    setdash("longdashed")
    drawnet(HorizontalLink(start = rightmid(layer_7), finish = leftmid(layer_83)))
    setdash("solid")

    text_label = Label(str = "4608 X 1", display = true)

    layer_9 = Tensor2D(position = Point(decoder_base + 70 , y), h_scale = 0.0035*factor, n_element_h = 1, n_element_v = 4608//factor, text_label = text_label)
    drawnet(layer_9)
    drawnet(HorizontalLink(start = rightmid(layer_81), finish = leftmid(layer_9)))
    setdash("longdashed")
    drawnet(HorizontalLink(start = rightmid(layer_82), finish = leftmid(layer_9)))
    drawnet(HorizontalLink(start = rightmid(layer_83), finish = leftmid(layer_9)))
    setdash("solid")

    text_label = Label(str = "9 X 1 X 512", display = true)
    
    layer_10 = Tensor3D(position = Point(decoder_base + 140, y), h_scale = 0.5, n_element_h = 1, n_element_v = 9, n_stack = 512//factor, x_offset_factor=factor*0.01, y_offset_factor= factor*0.01 , text_label = text_label)
    drawnet(layer_10)
    drawnet(HorizontalLink(start = rightmid(layer_9), finish = leftmid(layer_10), color = palette[3]))

    text_label = Label(str = "18 X 1 X 512", display = true)

    layer_11 = Tensor3D(n_element_h = 1, n_element_v = 18, position = Point(decoder_base + 240, y), h_scale = 0.5, n_stack = 512//factor, x_offset_factor=factor*0.01, y_offset_factor= factor*0.01, text_label = text_label)
    
    drawnet(layer_11)
    drawnet(HorizontalLink(start = rightmid(layer_10), finish = leftmid(layer_11), color = palette[6] ))

    text_label = Label(str = "18 X 1 X 256", display = true)

    layer_12 = Tensor3D(n_element_h = 1, n_element_v = 18, n_stack = 256//factor, position = Point(decoder_base + 340, y), h_scale = 0.5, x_offset_factor=factor*0.01, y_offset_factor= factor*0.01, text_label = text_label)
    drawnet(layer_12)
    drawnet(HorizontalLink(start = rightmid(layer_11), finish = leftmid(layer_12), color = palette[11] ))

    text_label = Label(str = "36 X 1 X 256", display = true)

    layer_13 = Tensor3D(n_element_h = 1, n_element_v = 36, n_stack = 256//factor, position = Point(decoder_base + 420, y), h_scale = 0.5, x_offset_factor=factor*0.01, y_offset_factor=factor*0.01, text_label= text_label)
    drawnet(layer_13)
    drawnet(HorizontalLink(start = rightmid(layer_12), finish = leftmid(layer_13), color = palette[6] ))

    text_label = Label(str = "36 X 1 X 256", display = true)

    layer_14 = Tensor2D(position = Point(decoder_base + 500, y), h_scale = 0.5, n_element_h = 1, n_element_v = 36)
    drawnet(layer_14)
    drawnet(HorizontalLink(start = rightmid(layer_13), finish = leftmid(layer_14), color = palette[11] ))

    # # element = Element(position = Point(decoder_base + 550, y), h_scale = 0.5)
    # # layer_15 = Tensor2D(element = element, n_element_h = 1, n_element_v = 36, y=0)
    # # drawnet(layer_15)
    # # drawnet(HorizontalLink(start = rightmid(layer_14), finish = leftmid(layer_15)))
    alpha = 0.9


    element = Element()
    layer_161 = Tensor2D(
        position = Point(decoder_base + 600, -220), h_scale = 0.5,
        n_element_h = 1,
        n_element_v = 36,
        color = [add_alpha(palette[12], alpha)],
    )
    drawnet(layer_161)
    drawnet(HorizontalLink(start = rightmid(layer_14), finish = leftmid(layer_161), color = palette[1]))

    element = Element()
    layer_162 = Tensor2D(
        position = Point(decoder_base + 600, y), h_scale = 0.5,
        n_element_h = 1,
        n_element_v = 36,
        color = [add_alpha(palette[6], alpha)],
    )

    drawnet(layer_162)
    setdash("longdashed")
    drawnet(HorizontalLink(start = rightmid(layer_14), finish = leftmid(layer_162), color = palette[1]))
    setdash("solid")

    layer_163 = Tensor2D(
        position = Point(decoder_base + 600, 220), h_scale = 0.5,
        n_element_h = 1,
        n_element_v = 36,
        color = [add_alpha(palette[10], alpha)],
    )

    drawnet(layer_163)
    setdash("longdashed")

    drawnet(HorizontalLink(start = rightmid(layer_14), finish = leftmid(layer_163), color = palette[1]))
    setdash("solid")

    setline(1.0)
    setcolor("black")
    setdash("dotdashed")
    b = box(
        Point(-615, -270),
        Point(-150, 270),
        action = :strokepath,
    )
    b = box(
        Point(25, -270),
        Point(560, 270),
        action = :strokepath,
    )

    strokepath()
    sethue("black")
    text("Encoder", Point(-420, -280))
    text("Decoder", Point(300, -280))

    x_start = -600
    y = 310
    setdash("solid")
    drawnet(HorizontalLink(start = Point(x_start,y), finish = Point(x_start+50,y ), color = palette[1] ))
    multilinetext(["2D Convolutions"], Point(x_start+70, y))

    drawnet(HorizontalLink(start = Point(x_start+200,y), finish = Point(x_start+250,y ), color = palette[2] ))
    multilinetext(["Max Pooling", "Batch Normalization", "Leaky ReLU"], Point(x_start+270, y))

    drawnet(HorizontalLink(start = Point(x_start+420,y), finish = Point(x_start+470,y ), color = palette[3] ))
    multilinetext(["Reshape"], Point(x_start+490, y))

    drawnet(HorizontalLink(start = Point(x_start+570,y), finish = Point(x_start+620,y )))
    multilinetext(["Dense"], Point(x_start+640, y))

    drawnet(HorizontalLink(start = Point(x_start+700,y), finish = Point(x_start+750,y ), color = palette[6]))
    multilinetext(["2D Upsampling"], Point(x_start+770, y))

    drawnet(HorizontalLink(start = Point(x_start+900,y), finish = Point(x_start+950,y ), color = palette[11]))
    multilinetext(["Transposed Convolutions", "Batch Normalization", "LeakyReLU"], Point(x_start+970, y))

end 1400 700 "/Users/anand/Documents/data/forest-mortality/images/vae"
