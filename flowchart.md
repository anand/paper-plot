```mermaid

flowchart LR

    era5[(ERA5 Data)] -.-> awegen(AWE-GEN)
    awegen --> wv[Weather Variables]
    wv --> formind
    wv --> vae(VAE)
    
    
    
    subgraph xai[Explainable AI]
        vae-->p[Prototypes]
        p -->plus
        plus --> p_selected[Extreme Biomass Loss \n Prototypes]
    end

   
    formind[FORMIND] --> m[Biomass Loss]
    m -->plus((('')))

    style awegen fill:#F0A58F,stroke:#7D3AC1,stroke-width:2px
    style formind fill:#F0A58F,stroke:#7D3AC1,stroke-width:2px
    style vae fill:#F0A58F,stroke:#7D3AC1,stroke-width:2px
    style era5 fill:#FCEAE6,stroke:#7D3AC1,stroke-width:2px
    
    style m fill:#FCEAE6,stroke:#7D3AC1,stroke-width:2px
    style p fill:#FCEAE6,stroke:#7D3AC1,stroke-width:2px
    style p_selected fill:#FCEAE6,stroke:#7D3AC1,stroke-width:2px
    style wv fill:#FCEAE6,stroke:#7D3AC1,stroke-width:2px
    style plus fill:#FCEAE6,stroke:#7D3AC1,color:#FCEAE6,stroke-width:2px

    style xai fill:#FF828,stroke:#AF4BCE,stroke-width:2px
    style xai fill:#F0F8FF,stroke:#AF4BCE,stroke-width:2px

```

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

 <!-- parameters[(Parameters)] -.-> formind(FORMIND) -->

 style parameters fill:#FFFFE0,stroke:#7D3AC1,stroke-width:2px