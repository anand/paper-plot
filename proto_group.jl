using NPZ
using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using MakieTeX
using DataFrames
using Combinatorics
## Some color palette

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 1:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.tol_light.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])

## Load the data


function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto_$(pft).npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end


x_beech, y_beech, z_beech, r_beech, p_beech, w_beech = read_data("beech")
x_pine, y_pine, z_pine, r_pine, p_pine, w_pine = read_data("pine")
x_spruce, y_spruce, z_spruce, r_spruce, p_spruce, w_spruce = read_data("spruce")
palette = ColorSchemes.tol_light.colors

## Do the work here

z = z_beech
y = y_beech

function get_data_upset(z)

    index = [1:size(z)[1];]

    index_5 = index[z[:, 14].<-2]
    index_4 = index[z[:, 12].<-2]
    index_3 = index[z[:, 22].>2]
    index_2 = index[(z[:, 14].<-1).&(z[:, 22].>1)]
    index_1 = index[(z[:, 12].<-1).&(z[:, 22].>1)]

    length(index_1) + length(index_2) + length(index_3) + length(index_4)

    s_1 = Set(index_1)
    s_2 = Set(index_2)
    s_3 = Set(index_3)
    s_4 = Set(index_4)
    s_5 = Set(index_5)

    union(s_1, s_2, s_3, s_4, s_5)

    set_arr = [s_1, s_2, s_3, s_4, s_5]

    intersect_groups = [
        [1],       # 1. a only    
        [2],       # 2. b only
        [3],       # 3. c only
        [4],
        [5],       # 4. d only
        [1, 5],     # 5. a and b only
        [2, 4],
        [3, 4],
        [3, 5],     # 6. a and c only
    ]

    intersect_groups = [c for c in combinations([1, 2, 3, 4, 5])]

    ycols = latexstring.(["Pz_{12-22}", "Pz_{14-22}", "Pz_{22}", "Pz_{12}", "Pz_{14}"])

    return set_arr, intersect_groups, ycols

end
# median(y[z[:,13].<-2])
# median(y[z[:,12].<-2])
# median(y[z[:,10].<-2])
# median(y[z[:,3].<-2])



function count_set(set_arr, intersect_groups)
    [length(intersect(getindex(set_arr, each)...)) for each in intersect_groups]
end


function upset_dots!(ax, colsets, nsets = maximum(Iterators.flatten(colsets)))
    xlims!(ax, (0, length(colsets) + 1))
    hidexdecorations!(ax)
    hidespines!(ax)

    for (x, p) in enumerate(colsets)
        scatter!(
            ax,
            fill(x, nsets - length(p)),
            collect(1:nsets)[Not(p)],
            markersize = 16,
            color = :lightgray,
        )
        scatter!(ax, fill(x, length(p)), p, markersize = 16, color = :black)
        length(p) > 1 && lines!(ax, [x, x], [extrema(p)...], color = :black, linewidth = 3)
    end
end

function upset(set_arr, intersect_groups, ycols)

    # fig = Figure(fontsize = 18, resolution = (length(intersect_groups)/1.5*100, 800) )
    fig = Figure(fontsize = 18, resolution = (1400, 800))

    intersection_ax_1 = Axis(
        fig[1, 1],
        ylabel = "Beech (Intersection)",
        yautolimitmargin = (0, 0.25),
        yticksvisible = false,
        yticklabelsvisible = false,
    )
    intersection_ax_2 = Axis(
        fig[2, 1],
        ylabel = "Pine (Intersection)",
        yautolimitmargin = (0, 0.25),
        yticksvisible = false,
        yticklabelsvisible = false,
    )
    intersection_ax_3 = Axis(
        fig[3, 1],
        ylabel = "Spruce (Intersection)",
        yautolimitmargin = (0, 0.25),
        yticksvisible = false,
        yticklabelsvisible = false,
    )

    dot_ax =
        Axis(fig[4, 1], xlabel = "set size", yticks = (1:length(ycols), string.(ycols)))
    # set_ax = Axis(fig[2,2], xlabel="set size", yticks = (1:length(ycols), string.(ycols)),
    #     xautolimitmargin = (0, 0.15), yticklabelpad = 10, xgridvisible = false)


    barplot!(
        intersection_ax_1,
        1:length(intersect_groups),
        count_set(set_arr[1], intersect_groups),
        bar_labels = :y,
        color = :gray20,
        label_size = 14,
        label_formatter = x -> string(Int(x)),
    )

    barplot!(
        intersection_ax_2,
        1:length(intersect_groups),
        count_set(set_arr[2], intersect_groups),
        bar_labels = :y,
        color = :gray20,
        label_size = 14,
        label_formatter = x -> string(Int(x)),
    )


    barplot!(
        intersection_ax_3,
        1:length(intersect_groups),
        count_set(set_arr[3], intersect_groups),
        bar_labels = :y,
        color = :gray20,
        label_size = 14,
        label_formatter = x -> string(Int(x)),
    )

    # barplot!(set_ax, 1:length(ycols), [length(each_set) for each_set in set_arr], 
    # direction=:x, bar_labels=:y, label_size = 14, color = :gray20,
    # label_formatter = x -> string(Int(x)))

    for i = 1:2:length(ycols)
        poly!(
            dot_ax,
            BBox(0, length(intersect_groups) + 1, i - 0.5, i + 0.5),
            color = :gray95,
        )
    end

    upset_dots!(dot_ax, intersect_groups)
    hidexdecorations!(intersection_ax_1)
    hidexdecorations!(intersection_ax_2)
    hidexdecorations!(intersection_ax_3)

    # hideydecorations!(set_ax, ticklabels = false)

    rowgap!(fig.layout, 0)
    # linkyaxes!(dot_ax, set_ax)
    linkxaxes!(dot_ax, intersection_ax_1)
    linkxaxes!(dot_ax, intersection_ax_2)
    linkxaxes!(dot_ax, intersection_ax_3)

    hidespines!(intersection_ax_1, :t, :r, :b)
    hidespines!(intersection_ax_2, :t, :r, :b)
    hidespines!(intersection_ax_3, :t, :r, :b)

    # hidespines!(set_ax, :t, :r, :l)

    fig

end



set_arr_1, intersect_groups, ycols = get_data_upset(z_beech)

set_arr_2, intersect_groups, ycols = get_data_upset(z_pine)

set_arr_3, intersect_groups, ycols = get_data_upset(z_spruce)

f = upset([set_arr_1, set_arr_2, set_arr_3], intersect_groups, ycols)
f
save("/Users/anand/Documents/data/forest-mortality/images/upset.png", f, px_per_unit = 5)

size(z_beech)

4371 / 126976

3237 / 126976

2883 / 126976
2256 / 126976
