using NPZ
using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using MakieTeX

## Some color palette 

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 2:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.mk_12.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])

## Load the data

function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto_$(pft).npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end


x_beech, y_beech, z_beech, r_beech, p_beech, w_beech = read_data("beech")
x_pine, y_pine, z_pine, r_pine, p_pine, w_pine = read_data("pine")
x_spruce, y_spruce, z_spruce, r_spruce, p_spruce, w_spruce = read_data("spruce")

## Do Some plots

function xtickformat(x)
    month_name = Dates.monthname.(Int.(x .% 12))

    for (i, m) in enumerate(x)
        year = Int(3 - floor(m / 12) - 1)

        if year == 0
            month_name[i] = month_name[i][1:3] * "_{t}"
        else
            month_name[i] = month_name[i][1:3] * "_{t$(-year)}"
        end
    end
    latexstring.(month_name)
end

fontsize_theme = Theme(fontsize = 22)
set_theme!(fontsize_theme)


f = Figure(resolution = (1400, 800))

# label_rad = [L"Pz_{13} \ [\frac{\mu mol}{m^2*s}]",L"Pz_{10}:Rad", L"Pz_{12}:Rad", L"Pz_{3}:Rad"]
# label_precip = [latexstring("Pz_{13}:Precip [\\frac{mm}{d}]"), latexstring("Pz_{10}:Radiation"), latexstring("Pz_{12}:Radiation"), latexstring("Pz_{3}:Radiation")]
# label_temp = [latexstring("Pz_{13}:Radiation"), latexstring("Pz_{10}:Radiation"), latexstring("Pz_{12}:Radiation"), latexstring("Pz_{3}:Radiation")]
axes_1 = [
    i < 6 ?
    Axis(
        f[i, 3],
        yticks = [-15:15:15;],
        xticks = 1:6:36,
        xgridvisible = false,
        ygridvisible = false,
        ylabelsize = 16,
        xticklabelsvisible = false,
    ) :
    Axis(
        f[i, 3],
        yticks = [-15:15:15;],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xticklabelrotation = π / 4,
        xgridvisible = false,
        ygridvisible = false,
        ylabelsize = 16,
    ) for i = 2:6
]
axes_2 = [
    i < 6 ?
    Axis(
        f[i, 5],
        yticks = [-0.5:0.5:0.5;],
        xticks = 1:6:36,
        xgridvisible = false,
        ygridvisible = false,
        ylabelsize = 16,
        xticklabelsvisible = false,
    ) :
    Axis(
        f[i, 5],
        yticks = [-0.25:0.5:0.25;],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xticklabelrotation = π / 4,
        xgridvisible = false,
        ygridvisible = false,
        ylabelsize = 16,
    ) for i = 2:6
]
axes_3 = [
    i < 6 ?
    Axis(
        f[i, 7],
        yticks = [-0.7:0.7:0.7;],
        xticks = 1:6:36,
        xgridvisible = false,
        ygridvisible = false,
        ylabelsize = 16,
        xticklabelsvisible = false,
    ) :
    Axis(
        f[i, 7],
        yticks = [-0.75:0.75:0.75;],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xticklabelrotation = π / 4,
        xgridvisible = false,
        ygridvisible = false,
        ylabelsize = 16,
    ) for i = 2:6
]
f
Label(
    f[2:end, 2],
    L"Radiation \  [[\mu mol^{-1}m^{-2}s^{-1}]",
    rotation = pi / 2,
    tellheight = false,
)
Label(
    f[2:end, 4],
    L"Precipitation \  [[mm \ - d^{-1}]",
    rotation = pi / 2,
    tellheight = false,
)
Label(f[2:end, 6], L"Temperature \  [[°C]", rotation = pi / 2, tellheight = false)


f
n_prot = 5
p = p_beech
for j = 1:n_prot
    dim_list = [1, 2, 3]
    # if j==1
    #     dim_list[i] = 10
    # elseif j==2
    #     dim_list[i] =13
    # elseif j==3
    #     dim_list[i]=2
    # elseif j==4
    #     dim_list[i] = 9
    # elseif j ==5
    #     dim_list[i] = 12
    # else
    #     dim_list[i] = 3
    # end

    if j>3
        p2=3
        if j==4
            p1 = 1
        else
            p1 = 2
        end            
        for i = 3:3
            if i == 1
                alpha = 1
                l = latexstring("z_{$(dim_list[i])}^{ 0}")
                linestyle = :dashdotdot
            elseif i == 2
                l = latexstring("z_{$(dim_list[i])}^{ \\sigma}")
                alpha = 1
                linestyle = :dash
            elseif i == 3
                alpha = 1
                l = latexstring("z_{$(dim_list[i])}^{ 2\\sigma}")
                linestyle = :solid
            end
    
            lines!(
                axes_1[j],
                p[p1, :, 1, 2],
                color = (palette[12], alpha),
                label = l,
                linestyle = linestyle,
            )
            axes_1[j].yticks = [-7:7:7;]

            ylims!(axes_1[j], (-9, 9))
            hlines!(axes_1[j], 0, color = :black, linestyle = "--")
            vlines!(axes_1[j], [12, 24], color = :black, linestyle = "--")
            hidespines!(axes_1[j], :r, :t)
    
            lines!(
                axes_2[j],
                p[p1, :, 2, 2],
                color = (palette[6], alpha),
                label = l,
                linestyle = linestyle,
            )
            axes_2[j].yticks = [-0.25:0.25:0.25;]

            ylims!(axes_2[j], (-0.4, 0.4))
            hlines!(axes_2[j], 0, color = :black, linestyle = "--")
            vlines!(axes_2[j], [12, 24], color = :black, linestyle = "--")
            hidespines!(axes_2[j], :r, :t)
    
            lines!(
                axes_3[j],
                p[p2, :, 3, 2],
                color = (palette[10], alpha),
                label = l,
                linestyle = linestyle,
            )
            axes_3[j].yticks = [-0.3:0.3:0.3;]
            ylims!(axes_3[j], (-0.4, 0.4))
            hlines!(axes_3[j], 0, color = :black, linestyle = "--")
            vlines!(axes_3[j], [12, 24], color = :black, linestyle = "--")
            hidespines!(axes_3[j], :r, :t)
        end
    else
        for i = 3:3
            if i == 1
                alpha = 1
                l = latexstring("z_{$(dim_list[i])}^{ 0}")
                linestyle = :dashdotdot
            elseif i == 2
                l = latexstring("z_{$(dim_list[i])}^{ \\sigma}")
                alpha = 1
                linestyle = :dash
            elseif i == 3
                alpha = 1
                l = latexstring("z_{$(dim_list[i])}^{ 2\\sigma}")
                linestyle = :solid
            end
    
            # print(linestyle * "\n")
    
            lines!(
                axes_1[j],
                p[dim_list[j], :, 1, i],
                color = (palette[12], alpha),
                label = l,
                linestyle = linestyle,
            )
            ylims!(axes_1[j], (-18, 18))
            hlines!(axes_1[j], 0, color = :black, linestyle = "--")
            vlines!(axes_1[j], [12, 24], color = :black, linestyle = "--")
            hidespines!(axes_1[j], :r, :t)
    
            lines!(
                axes_2[j],
                p[dim_list[j], :, 2, i],
                color = (palette[6], alpha),
                label = l,
                linestyle = linestyle,
            )
            ylims!(axes_2[j], (-0.8, 0.8))
            hlines!(axes_2[j], 0, color = :black, linestyle = "--")
            vlines!(axes_2[j], [12, 24], color = :black, linestyle = "--")
            hidespines!(axes_2[j], :r, :t)
    
            lines!(
                axes_3[j],
                p[dim_list[j], :, 3, i],
                color = (palette[10], alpha),
                label = l,
                linestyle = linestyle,
            )
            ylims!(axes_3[j], (-0.8, 0.8))
            hlines!(axes_3[j], 0, color = :black, linestyle = "--")
            vlines!(axes_3[j], [12, 24], color = :black, linestyle = "--")
            hidespines!(axes_3[j], :r, :t)
    
        end
    end

end

f

elem_1 = [LineElement(color = palette[12], linestyle = nothing)]
elem_2 = [LineElement(color = palette[6], linestyle = nothing)]
elem_3 = [LineElement(color = palette[10], linestyle = nothing)]

elem_4 = [LineElement(color = :gray, linestyle = :dashdotdot)]
elem_5 = [LineElement(color = :gray, linestyle = :dash)]
elem_6 = [LineElement(color = :gray, linestyle = :solid)]

Legend(
    f[1, 3:end],
    [elem_1, elem_2, elem_3], #, elem_4, elem_5, elem_6],
    [
        "Radiation",
        "Precipitation",
        "Temperature",
        # latexstring("P_{i|0\\sigma}^{}"),
        # latexstring("P_{i|1\\sigma}^{}"),
        # latexstring("P_{i|2\\sigma}^{}"),
    ],
    patchsize = (25, 25),
    rowgap = 10,
    nbanks = 6,
    framevisible = false,
    orientation = :vertical,
)
f
proto_elem = [[LineElement(color = :white, linestyle = nothing)] for i in [14, 12, 22, "14-22", "12-22"]]
f
Legend(
    f[2:end, 1],
    proto_elem,
    latexstring.(["P_{$(i)}" for i in ["14|2\\sigma", "12|2\\sigma", "22|2\\sigma", "14\\wedge22|\\sigma", "12\\wedge22|\\sigma"]]),
    patchsize = (0, 120),
    framevisible = false,
)
f
colsize!(f.layout, 1, Aspect(1, 0.5))
f

save(
    "/Users/anand/Documents/data/forest-mortality/images/z_variation_p.pdf",
    f
)
