using NPZ
using CairoMakie

using StatsBase
using ColorSchemes
using Colors
using MakieTeX


function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto_$(pft).npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end


x_beech, y_beech, z_beech, r_beech, p_beech, w_beech = read_data("beech")
x_pine, y_pine, z_pine, r_pine, p_pine, w_pine = read_data("pine")
x_spruce, y_spruce, z_spruce, r_spruce, p_spruce, w_spruce = read_data("spruce")
palette = ColorSchemes.tol_light.colors

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 2:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.tol_light.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])


function SymLogRange(stop, length)
    print(round(length / 2))
    bin_1 = [-x for x in LogRange(0.05, stop, Int(round(length / 2)))]
    bin_2 = [x for x in LogRange(0.05, stop, Int(round(length / 2)))]

    return vcat(reverse(bin_1), bin_2)
end

function LogRange(start, stop, length)
    (2^(y) for y in range(log2(start), stop = log2(stop), length = length))
end


######### Figure below

function plot_bp!(ax_box, ax_dist, z, y)
    perc = 90
    latent_dim = 32

    z_non_extreme = z[y.<=percentile(y, perc), :]
    z_extreme = z[y.>percentile(y, perc), :]
    xaxis_extreme =
        transpose(repeat((Float32.([1.25:2:2*latent_dim+1;])), 1, size(z_extreme)[1]))
    xaxis_non_extreme =
        transpose(repeat((Float32.([0.75:2:2*latent_dim;])), 1, size(z_non_extreme)[1]))

    diff_latent = abs.((mean(z_non_extreme, dims = 1) .- mean(z_extreme, dims = 1)))[1, :]
    sorted_diff_latent = sort(diff_latent, rev = true)
    x_sorted = sortperm(diff_latent, rev = true)

    z_n = 3
    k = 1
    # b1 = rainclouds!(ax_box, xaxis_non_extreme[:,k], z_non_extreme[:,x_sorted[k]],  width = 0.5, color = p_base[7], show_outliers = false, side_nudge = 0.075, markersize=0.5 )
    b1 = boxplot!(
        ax_box,
        xaxis_non_extreme[:, k],
        z_non_extreme[:, x_sorted[k]],
        width = 0.5,
        color = p_base[7],
        show_outliers = false,
    )
    # b2 = rainclouds!(ax_box, xaxis_extreme[:,k], z_extreme[:,x_sorted[k]], width = 0.5,color = (p_base[2], 0.7), show_outliers = false, side = :right, side_nudge = 0.075, markersize=0.5)

    b2 = boxplot!(
        ax_box,
        xaxis_extreme[:, k],
        z_extreme[:, x_sorted[k]],
        width = 0.5,
        color = p_base[2],
        show_outliers = false,
    )
    for k = 2:z_n
        # print(x_sorted[k])
        # vspan!(ax_box, [1:2:2*latent_dim;][x_sorted[k]]-0.5, [1:2:2*latent_dim;][x_sorted[k]]+0.5, color = (p_base[2], 0.15))
        # rainclouds!(ax_box, xaxis_non_extreme[:,k], z_non_extreme[:,x_sorted[k]],  width = 0.5, color = p_base[7], show_outliers = false, side_nudge = 0.075, markersize=0.5 )
        # rainclouds!(ax_box, xaxis_extreme[:,k], z_extreme[:,x_sorted[k]], width = 0.5,color = (p_base[2], 0.7), show_outliers = false, side = :right, side_nudge = 0.075, markersize=0.5)

        boxplot!(
            ax_box,
            xaxis_non_extreme[:, k],
            z_non_extreme[:, x_sorted[k]],
            width = 0.5,
            color = p_base[7],
            show_outliers = false,
        )
        boxplot!(
            ax_box,
            xaxis_extreme[:, k],
            z_extreme[:, x_sorted[k]],
            width = 0.5,
            color = p_base[2],
            show_outliers = false,
        )
    end

    # i = 1
    # b1 = boxplot!(ax_box, xaxis_non_extreme[:,i], z_non_extreme[:,i],  width = 0.5, color = p_base[7], show_outliers = false, label = "non-extreme")
    # b2 = boxplot!(ax_box, xaxis_extreme[:,i], z_extreme[:,i], width = 0.5,color = p_base[2], show_outliers = false, label = "extreme")

    # for i in 2:latent_dim
    #     boxplot!(ax_box, xaxis_non_extreme[:,i], z_non_extreme[:,i],  width = 0.5, color = p_base[7], show_outliers = false)
    #     boxplot!(ax_box, xaxis_extreme[:,i], z_extreme[:,i], width = 0.5,color = p_base[2], show_outliers = false)

    # end

    ax_box.xticks = ([1:2:2*z_n;], [string(x_sorted[i]) for i = 1:z_n])
    xlims!(ax_box, (0.25, 2 * z_n - 0.25))
    ylims!(ax_box, (-5, 5))



    color_list = zeros(16)
    color_list[1:4] .= 1

    s1 = scatterlines!(ax_dist, [1:z_n;], sorted_diff_latent[1:z_n], color = p_base[2])
    scatterlines!(
        ax_dist,
        [z_n:latent_dim;],
        sorted_diff_latent[z_n:end],
        color = (p_base[2], 0.3),
    )

    ax_dist.xticks = ([1:latent_dim;], [string(each) for each in x_sorted])
    hidespines!(ax_box, :t, :r)
    hidespines!(ax_dist, :t, :r)
    Legend(
        f[1, 5:6],
        [b1, b2],
        ["Non-Extreme", "Extreme"],
        orientation = :horizontal,
        framevisible = false,
    )
    Legend(
        f[1, 2:4],
        [s1],
        ["Selected Dimensions"],
        orientation = :horizontal,
        framevisible = false,
    )

end



fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)

f = Figure(resolution = (1400, 800))
Label(f[2, 1], "Beech", rotation = pi / 2, 
tellheight = false,
# font = "TeX Gyre Heros Bold",
textsize = 22)
Label(f[3, 1], "Pine", rotation = pi / 2, tellheight = false, textsize = 22)
Label(f[4, 1], "Spruce", rotation = pi / 2, tellheight = false, textsize = 22)

ax_box_beech = Axis(
    f[2, 5:6],
    ylabel = L"z",
    xgridvisible = false,
    ygridvisible = false,
    xticklabelrotation = pi / 2,
    ylabelsize = 22

)
ax_box_pine = Axis(
    f[3, 5:6],
    ylabel = L"z",
    xgridvisible = false,
    ygridvisible = false,
    xticklabelrotation = pi / 2,
    ylabelsize = 22
)
ax_box_spruce = Axis(
    f[4, 5:6],
    xlabel = "Latent dimensions",
    ylabel = L"z",
    xgridvisible = false,
    ygridvisible = false,
    xticklabelrotation = pi / 2,
    ylabelsize = 22
)

ax_dist_beech = Axis(
    f[2, 2:4],
    ylabel = "Absolute Mean\nDifference [-]",
    xgridvisible = false,
    ygridvisible = false,
    xticklabelrotation = pi / 2,
)
ax_dist_pine = Axis(
    f[3, 2:4],
    ylabel = "Absolute Mean\nDifference [-]",
    xgridvisible = false,
    ygridvisible = false,
    xticklabelrotation = pi / 2,
)
ax_dist_spruce = Axis(
    f[4, 2:4],
    xlabel = "Latent dimensions",
    ylabel = "Absolute Mean\nDifference [-]",
    xgridvisible = false,
    ygridvisible = false,
    xticklabelrotation = pi / 2,
)

f
plot_bp!(ax_box_beech, ax_dist_beech, z_beech, y_beech)
f
plot_bp!(ax_box_pine, ax_dist_pine, z_pine, y_pine)
f
plot_bp!(ax_box_spruce, ax_dist_spruce, z_spruce, y_spruce)
f
left = 90
top = -0

Label(
        f[2, 2, TopLeft()],
        "a)",
        font = "TeX Gyre Heros Bold",
        textsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )


Label(
    f[3, 2, TopLeft()],
    "c)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)

Label(
    f[4, 2, TopLeft()],
    "e)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)

left = 60


Label(
    f[2, 5, TopLeft()],
    "b)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)

Label(
    f[3, 5, TopLeft()],
    "d)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)



Label(
    f[4, 5, TopLeft()],
    "f)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)
f

save("/Users/anand/Documents/data/forest-mortality/images/bp.pdf", f)

# scatter!(ax_3d, z[:,13], z[:,10], z[:,12], color = log2.(y),  colormap = cmap_base, markersize = 3)

function plot_z_mort!(ax, legend_pos, z, y, lims)
    perc = 90
    latent_dim = 32
    z_non_extreme = z[y.<=percentile(y, perc), :]
    z_extreme = z[y.>percentile(y, perc), :]

    diff_latent = abs.((mean(z_non_extreme, dims = 1) .- mean(z_extreme, dims = 1)))[1, :]
    x_sorted = sortperm(diff_latent, rev = true)

    z_n = 3

    for index = 1:z_n
        bins = SymLogRange(maximum(z[:, x_sorted[index]]), 15)
        bins = [-2.5:0.5:2.5;]
        h1 = fit(Histogram, z[:, x_sorted[index]], bins)
        xmap1 = StatsBase.binindex.(Ref(h1), z[:, x_sorted[index]])

        fm = zeros(size(bins)[1])
        for i = 1:size(bins)[1]
            if isempty(y[xmap1.==i])
                fm[i] = NaN
            else
                fm[i] = median(y[xmap1.==i])
            end
        end

        if index == 1
            p_index = 3
        elseif index == 2
            p_index = 4
        elseif index == 3
            p_index = 5
        elseif index == 4
            p_index = 8
        end

        lines!(
            ax,
            bins,
            fm,
            color = (p_base[2*index-1]),
            label = latexstring("z_{$(x_sorted[index])}"),
            linewidth = 2,
        )
    end
    print("Hello")
    hlines!(ax, median(y), color = :black, linestyle = "-.", label = "Median")
    xlims!(ax, (-2.5, 2.5))
    ylims!(ax, lims)
    hidespines!(ax, :r, :t)

    Legend(legend_pos, ax, orientation = :horizontal, framevisible = false, nbanks = 1)
end


f = Figure(resolution = (1400, 300))
ax_beech = Axis(
    f[2:4, 1:2],
    xlabel = latexstring("\$z\$ Beech"),
    ylabel = "Mortality [%]",
    xgridvisible = false,
    ygridvisible = false,
    yticks = [1.6:0.2:2.0;],
)
ax_pine = Axis(
    f[2:4, 3:4],
    xlabel = latexstring("\$z\$ Pine"),
    xgridvisible = false,
    ygridvisible = false,
    yticks = [2.3:0.2:2.7;],
)
ax_spruce = Axis(
    f[2:4, 5:6],
    xlabel = latexstring("\$z\$ Spruce"),
    xgridvisible = false,
    ygridvisible = false,
    yticks = [3.1:0.2:3.6;],
)

legend_beech = f[1, 1:2]
legend_pine = f[1, 3:4]
legend_spruce = f[1, 5:6]

plot_z_mort!(ax_beech, legend_beech, z_beech, y_beech, (1.6, 2.0))
f
plot_z_mort!(ax_pine, legend_pine, z_pine, y_pine, (2.3, 2.7))
f
plot_z_mort!(ax_spruce, legend_spruce, z_spruce, y_spruce, (3.1, 3.6))
f

Label(
    f[2, 1, TopLeft()],
    "a)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)

Label(
    f[2, 3, TopLeft()],
    "b)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)

Label(
    f[2, 5, TopLeft()],
    "c)",
    font = "TeX Gyre Heros Bold",
    textsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)
f

save("/Users/anand/Documents/data/forest-mortality/images/z_mort.pdf", f)
