using NPZ
using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using MakieTeX
using HypothesisTests
using GLM
using EvalMetrics


## Some color palette

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 1:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.tol_light.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])

## Load the data

data_path = "/Users/anand/Documents/data/forest-mortality/"

function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto_$(pft).npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end

x_beech, y_beech, z_beech, r_beech, p_beech, w_beech = read_data("beech")
x_pine, y_pine, z_pine, r_pine, p_pine, w_pine = read_data("pine")
x_spruce, y_spruce, z_spruce, r_spruce, p_spruce, w_spruce = read_data("spruce")


function lr(y, z)

    perc = 90
    z_n = 3
    z_non_extreme = z[y.<=percentile(y, perc), :]
    z_extreme = z[y.>percentile(y, perc), :]

    diff_latent = abs.((mean(z_non_extreme, dims = 1) .- mean(z_extreme, dims = 1)))[1, :]
    x_sorted = sortperm(diff_latent, rev = true)

    y_bin = zeros(size(y))

    y_bin[y.<=percentile(y, 90)] .= 0
    y_bin[y.>0] .= 1

    z = Float64.(z[:, x_sorted[1:z_n]])

    xprobit = glm(z[1:80000, :], y_bin[1:80000], Binomial(), ProbitLink())

    y_pred = predict(xprobit, z[80000:end, :])

    print(binary_eval_report(y[80000:end], y_pred))

    csi = 0
    f1 = 0
    for t = 0:0.01:1
        y_bin = zeros(size(y_pred))
        y_bin[y_pred.>t] .= 1
        y_bin[y_pred.<=t] .= 0

        conf_mat = ConfusionMatrix(y[80000:end], y_bin)
        tn = true_positive(conf_mat)
        tp = true_negative(conf_mat)
        fp = false_positive(conf_mat)
        fn = false_negative(conf_mat)

        csi = max(csi, tn / (tn + fp + fn))
        f1 = max(f1, 2 * tn / (2 * tn + fp + fn))

    end
    print("This is CSI $(csi) \n")
    print("This is F1 $(f1) \n")

end

lr(y_beech, z_beech)
