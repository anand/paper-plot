using NPZ
using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using MakieTeX
using DataFrames
using Combinatorics
using NCDatasets

## Some color palette

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 1:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.tol_light.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])

## Load the data


function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto_$(pft).npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end

function read_ERA()
    data_path = "/Users/anand/Documents/data/forest-mortality/"
    # print(NCDataset(data_path*"SWdown_WFDE5_CRU_mon_v1.1.nc")["lat"][283])
    # print(NCDataset(data_path*"SWdown_WFDE5_CRU_mon_v1.1.nc")["lon"][381])


    srad = NCDataset(data_path*"SWdown_WFDE5_CRU_mon_v1.1.nc")["SWdown"][381,283, :]
    precip = NCDataset(data_path*"Rainf_WFDE5_CRU_mon_v1.0.nc")["Rainf"][381,283, :]
    temp = NCDataset(data_path*"Tair_WFDE5_CRU_mon_v1.0.nc")["Tair"][381,283, :]

    return srad, precip, temp
end


x_beech, y_beech, z_beech, r_beech, p_beech, w_beech = read_data("beech")

srad, precip, temp = read_ERA()

srad = reshape(srad, 12, 40)
precip = reshape(precip, 12, 40)
temp = reshape(temp, 12, 40)

size(x_beech)


corr_mat_sim = zeros(12, 3)
corr_mat_era5 = zeros(12, 3)

for i=1:12
    corr_mat_sim[i, 1] = crosscor(x_beech[:, i, 1, 1], x_beech[:, i, 2, 1], [0])[1]
    corr_mat_sim[i, 2] = crosscor(x_beech[:, i, 1, 1], x_beech[:, i, 3, 1], [0])[1]
    corr_mat_sim[i, 3] = crosscor(x_beech[:, i, 2, 1], x_beech[:, i, 3, 1], [0])[1]

    corr_mat_era5[i, 1] = crosscor(Float32.(srad[i, :]), Float32.(precip[i, :]), [0])[1]
    corr_mat_era5[i, 2] = crosscor(Float32.(srad[i, :]), Float32.(temp[i, :]), [0])[1]
    corr_mat_era5[i, 3] = crosscor(Float32.(temp[i, :]), Float32.(precip[i, :]), [0])[1]

end

fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)

function xtickformat(x)
    
    return ["Rad-Precip", "Rad-Temp", "Precip-Temp" ]
    
end

f = Figure(resolution=(1400,800))
ax_era5 = Axis(f[1,1], xticks = 1:3, xtickformat = xtickformat)
h_era5 = heatmap!(ax_era5, transpose(corr_mat_era5), colormap = :RdBu,  colorrange = (-0.5, 0.5))
Colorbar(f[1, 2], h_era5; label = "values", width = 15, ticksize = 15)

ax_sim = Axis(f[1,3], xticks = 1:3, xtickformat = xtickformat)
h_sim = heatmap!(ax_sim, transpose(corr_mat_sim), colormap = :RdBu,  colorrange = (-0.5, 0.5))
Colorbar(f[1, 4], h_sim; label = "values", width = 15, ticksize = 15)
f
