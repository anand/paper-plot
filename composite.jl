using NPZ
using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using MakieTeX
using DataFrames
using Combinatorics
## Some color palette

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 1:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.mk_12.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])


function xtickformat(x)
    month_name = Dates.monthname.(Int.(x .% 12))

    for (i, m) in enumerate(x)
        year = Int(3 - floor(m / 12) - 1)

        if year == 0
            month_name[i] = month_name[i][1:3] * "_{t}"
        else
            month_name[i] = month_name[i][1:3] * "_{t$(-year)}"
        end
    end
    latexstring.(month_name)
end


## Load the data


function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto.npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = permutedims(reshape(w_xd, (3, 36)), (2, 1))

    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end



x_beech, y_beech, _, _, _, w_beech = read_data("beech")
x_pine, y_pine, _, _, _, w_pine = read_data("pine")
x_spruce, y_spruce, _, _, _, w_spruce = read_data("spruce")

function get_composite(x, y)
    index = y .> percentile(y, 90)

    comp = mean(x[index, :, :], dims = 1)
    comp_std = std(x[index, :, :], dims = 1)

    return comp, comp_std
end

beech_comp, _ = get_composite(x_beech, y_beech)
pine_comp, _ = get_composite(x_pine, y_pine)
spruce_comp, _ = get_composite(x_spruce, y_spruce)

# min_max = (-0.8, 0.8)

## Assuming font 18 is good enough for 1400 resolution, let us make subplots 


function plot_individual(ax1, ax2, comp, w_xd)

    l1 = lines!(ax1, comp[1, :, 1], color = palette[12], label = "Radiation")
    l2 = lines!(ax1, comp[1, :, 2], color = palette[6], label = "Precipitation")
    l3 = lines!(ax1, comp[1, :, 3], color = palette[10], label = "Temperature")
    lines!(ax1, [0,36], [0,0], color="black", linestyle="--")
    s1 = lines!(ax2, [1:36;], w_xd[:, 1], color = palette[12], markersize = 15)
    s2 = lines!(ax2, [1:36;], w_xd[:, 2], color = palette[6], markersize = 15)
    s3 = lines!(ax2, [1:36;], w_xd[:, 3], color = palette[10], markersize = 15)
    lines!(ax2, [0,36], [0,0], color="black", linestyle="--")

    vlines!(ax1, [12, 24], color = :black, linestyle = "--")
    vlines!(ax2, [12, 24], color = :black, linestyle = "--")

    xlims!(ax1, (1, 36))
    xlims!(ax2, (1, 36))
    ylims!(ax1, (-0.5, 0.5))
    ylims!(ax2, (-1.0, 1.0))


    hidespines!(ax1, :t, :r)
    hidespines!(ax2, :t, :r)
end

function plot_complete()
    fontsize_theme = Theme(fontsize = 18)
    set_theme!(fontsize_theme)

    f = Figure(resolution = (1400, 900))
    ax1 = Axis(
        f[2, 2],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xgridvisible = false,
        ygridvisible = false,
        ylabel = "Anomaly [-]",
    )
    ax2 = Axis(
        f[2, 3],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xgridvisible = false,
        ygridvisible = false,
        ylabel = "Coefficients [-]",
    )

    ax3 = Axis(
        f[3, 2],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xgridvisible = false,
        ygridvisible = false,
        ylabel = "Anomaly [-]",
    )
    ax4 = Axis(
        f[3, 3],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xgridvisible = false,
        ygridvisible = false,
        ylabel = "Coefficients [-]",
    )

    ax5 = Axis(
        f[4, 2],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xgridvisible = false,
        ygridvisible = false,
        ylabel = "Anomaly [-]",
    )
    ax6 = Axis(
        f[4, 3],
        xticks = 1:6:36,
        xtickformat = xtickformat,
        xgridvisible = false,
        ygridvisible = false,
        ylabel = "Coefficients [-]",
    )

    plot_individual(ax1, ax2, beech_comp, w_beech)
    plot_individual(ax3, ax4, pine_comp, w_pine)
    plot_individual(ax5, ax6, spruce_comp, w_spruce)

    elem = [[LineElement(color = :white, linestyle = nothing)] for i in 1:3]
    Label(f[2, 1], "Beech", rotation = pi / 2, 
    tellheight = false,
    # font = "TeX Gyre Heros Bold",
    textsize = 22)
    Label(f[3, 1], "Pine", rotation = pi / 2, tellheight = false, textsize = 22)
    Label(f[4, 1], "Spruce", rotation = pi / 2, tellheight = false, textsize = 22)

    #     Legend(
#     f[2:4, 1],
#     elem,
#     ["Beech", "Pine", "Spruce"],
#     patchsize = (0, 168),
#     framevisible = false,
#     orientation = :vertical,
#     rotation = pi/2
# )

    Legend(f[1, :], ax1, orientation = :horizontal, framevisible = false)

    left = 60
    top = 5
    Label(
        f[2, 2, TopLeft()],
        "a)",
        font = "TeX Gyre Heros Bold",
        textsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )
    Label(
        f[2, 3, TopLeft()],
        "b)",
        font = "TeX Gyre Heros Bold",
        textsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )

    Label(
        f[3, 2, TopLeft()],
        "c)",
        font = "TeX Gyre Heros Bold",
        textsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )
    Label(
        f[3, 3, TopLeft()],
        "d)",
        font = "TeX Gyre Heros Bold",
        textsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )

    Label(
        f[4, 2, TopLeft()],
        "e)",
        font = "TeX Gyre Heros Bold",
        textsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )
    Label(
        f[4, 3, TopLeft()],
        "f)",
        font = "TeX Gyre Heros Bold",
        textsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )

    f

end

f = plot_complete()
f

save(
    "/Users/anand/Documents/data/forest-mortality/images/composite_coefficients.pdf",
    f,
)
