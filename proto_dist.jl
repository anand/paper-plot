using NPZ
# using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using LaTeXStrings
# using HypothesisTests
## Some color palette

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 1:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.tol_light.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])


## Load the data

function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto.npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end


x_beech, y_beech, z_beech, r_beech, p_beech, w_beech = read_data("beech")
x_pine, y_pine, z_pine, r_pine, p_pine, w_pine = read_data("pine")
x_spruce, y_spruce, z_spruce, r_spruce, p_spruce, w_spruce = read_data("spruce")

function plot_dist!(ax, z, y, lims)
    perc = 90

    offset = 6

    z_non_extreme = z[y.<=percentile(y, perc), :]
    z_extreme = z[y.>percentile(y, perc), :]

    diff_latent = (mean(z_non_extreme, dims = 1).-mean(z_extreme, dims = 1))[1, :]
    x_sorted = sortperm(abs.(diff_latent), rev = true)

    z_n = 3
    all_index = [1:size(z)[1];]

    d = density!(
        ax,
        y,
        offset = 2 * (offset - 1),
        color = (:gray80, 0.5),
        colorrange = (-20, 20),
        strokewidth = 1,
        strokecolor = :black,
    )
    lines!(
        ax,
        [median(y), median(y)],
        [2 * (offset - 1), 2 * (offset)],
        color = :gray20,
        linestyle = :dash,
    )
    lines!(
        ax,
        [percentile(y, perc), percentile(y, perc)],
        [2 * (offset - 1), 2 * (offset)],
        color = :gray20,
        linestyle = :dashdot,
    )

    
    # LABEL: $(round(median(y), digits = 2)) %

    for i = 1:z_n
        latent_index = x_sorted[i]
        if sign(diff_latent[latent_index]) == -1.0
            index = all_index[z[:, latent_index].>2]
        else
            index = all_index[z[:, latent_index].<-2]
        end

        d = density!(
            ax,
            y[index],
            offset = offset - 2 * (i - 2),
            color = (p_base[2*i-1], 0.3),
            colorrange = (-20, 20),
            strokewidth = 1,
            strokecolor = p_base[2*i-1],
        )
        # label = "Median Change : $(round(100*(median(y[index]) - median(y))/median(y), digits = 2)) %"
        print(" median δ is $((median(y[index])-median(y))*100/median(y))")
        # print("90th perc δ is  $((percentile(y[index], perc)-percentile(y, perc))*100/percentile(y, perc))")

        lines!(
            ax,
            [median(y[index]), median(y[index])],
            [2 * (offset - 1) - 2 * (i - 1), 2 * (offset - 1) - 2 * (i)],
            color = p_base[2*i-1],
            linestyle = :dash,
            linewidth = 2,
        )
        lines!(
            ax,
            [percentile(y[index], perc), percentile(y[index], perc)],
            [2 * (offset - 1) - 2 * (i - 1), 2 * (offset - 1) - 2 * (i)],
            color = p_base[2*i-1],
            linestyle = :dashdot,
            linewidth = 2,
        )

        # label = , label = "Median Mortality : $(round(median(y[index]), digits = 2)) %"

        xlims!(ax, lims)
        ylims!(ax, (0, 2 * offset))


        ## Also manually do pairwse

        hidespines!(ax, :r, :t)
    end
    l1 = 12
    l2 = 14
    l3 = 22

    t = 1

    index = all_index[(z[:, l2].<-t).&(z[:, l3].>t)]
    d = density!(
        ax,
        y[index],
        offset = 2,
        color = (p_base[7], 0.3),
        colorrange = (-20, 20),
        strokewidth = 1,
        strokecolor = p_base[7],
    )
    print(" median δ is $((median(y[index])-median(y))*100/median(y))")
    # print("90th perc δ is $((percentile(y[index], perc)-percentile(y, perc))*100/percentile(y, perc))")

    lines!(
        ax,
        [median(y[index]), median(y[index])],
        [2, 4],
        color = p_base[6],
        linestyle = :dash,
        linewidth = 2,
    )
    lines!(
        ax,
        [percentile(y[index], perc), percentile(y[index], perc)],
        [2, 4],
        color = p_base[7],
        linestyle = :dashdot,
        linewidth = 2,
    )

    index = all_index[(z[:, l1].<-t).&(z[:, l3].>t)]
    print(" median δ is $((median(y[index])-median(y))*100/median(y))")
    # print("90th perc δ is  $((percentile(y[index], perc)-percentile(y, perc))*100/percentile(y, perc))")

    d = density!(
        ax,
        y[index],
        offset = 0,
        color = (p_base[6], 0.3),
        colorrange = (-20, 20),
        strokewidth = 1,
        strokecolor = p_base[6],
    )

    lines!(
        ax,
        [median(y[index]), median(y[index])],
        [0, 2],
        color = p_base[6],
        linestyle = :dash,
        linewidth = 2,
    )
    lines!(
        ax,
        [percentile(y[index], perc), percentile(y[index], perc)],
        [0, 2],
        color = p_base[7],
        linestyle = :dashdot,
        linewidth = 2,
    )

end


ylabel_beech = ["P_{12-22|\\sigma}", "P_{14-22|\\sigma}", "P_{22|2\\sigma}", "P_{12|2\\sigma}", "P_{14|2\\sigma}", "All"]
ylabel_pine = ["P_{12-22|\\sigma}", "P_{14-22|\\sigma}", "P_{12|2\\sigma}", "P_{22|2\\sigma}", "P_{14|2\\sigma}", "All"]
ylabel_spruce = ["P_{12-22|\\sigma}", "P_{14-22|\\sigma}", "P_{12|2\\sigma}", "P_{22|2\\sigma}", "P_{14|2\\sigma}", "All"]


f = Figure(resolution = (1400, 500))

n_dist = 6

ax_beech = Axis(
    f[2, 1],
    xgridvisible = false,
    ygridvisible = false,
    yticks = ([1:2:2*n_dist;], latexstring.(ylabel_beech)),
    xlabel = "Biomass Loss [%] Beech ",
)
ax_pine = Axis(
    f[2, 2],
    xgridvisible = false,
    ygridvisible = false,
    yticks = ([1:2:2*n_dist;], latexstring.(ylabel_pine)),
    xlabel = "Biomass Loss [%] Pine",
)
ax_spruce = Axis(
    f[2, 3],
    xgridvisible = false,
    ygridvisible = false,
    yticks = ([1:2:2*n_dist;], latexstring.(ylabel_spruce)),
    xlabel = "Biomass Loss [%] Spruce",
)

lims_beech = (1, 7)
lims_pine = (1, 7)
lims_spruce = (1, 7)

f

plot_dist!(ax_beech, z_beech, y_beech, lims_beech)
plot_dist!(ax_pine, z_pine, y_pine, lims_pine)
plot_dist!(ax_spruce, z_pine, y_spruce, lims_spruce)

f

elem_1 = [LineElement(color = (:gray, 0.8), linestyle = :dash)]
elem_2 = [LineElement(color = (:gray, 0.8), linestyle = :dash)]

perc = 90
Legend(
    f[1, :],
    [elem_1, elem_2],
    ["Median", "$(perc) percentile"],
    orientation = :horizontal,
    framevisible = false,
)
f

save(
    "/Users/anand/Documents/data/forest-mortality/images/proto_density.pdf",
    f
)
