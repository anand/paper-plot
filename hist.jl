using NPZ
using CairoMakie
using StatsBase
using ColorSchemes
using Dates


function xtickformat(x)
    month_name = Dates.monthname.(Int.(x .% 12))

    for (i, m) in enumerate(x)
        year = Int(3 - floor(m / 12) - 1)

        if year == 0
            month_name[i] = month_name[i][1:3] * "_{t}"
        else
            month_name[i] = month_name[i][1:3] * "_{t$(-year)}"
        end
    end
    latexstring.(month_name)
end


## Load the data


function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    x = npzread(data_path * "x_train_$(pft).npy")
    y = 100 * npzread(data_path * "y_train_$(pft).npy") # Convert to percent
    z = npzread(data_path * "z_train_$(pft).npy")
    r = npzread(data_path * "r_train_$(pft).npy")
    p = npzread(data_path * "proto.npy")
    w_xd = npzread(data_path * "$(pft)_w_logisitic_xd.npy")
    # w_xd_xs = npzread(data_path * "$(pft)_w_logisitic_xd_xs.npy")[:,1:108]

    w_xd = reverse(transpose(reshape(w_xd, (3, 36))), dims = 2)
    # w_xd_xs = reverse(transpose(reshape(w_xd_xs, (3, 36))), dims = 2)

    return x, y, z, r, p, w_xd

end


x_beech, y_beech, _, _, _, w_beech = read_data("beech")
x_pine, y_pine, _, _, _, w_pine = read_data("pine")
x_spruce, y_spruce, _, _, _, w_spruce = read_data("spruce")

kurtosis(y_beech)
kurtosis(y_pine)
kurtosis(y_spruce)


p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)


palette = ColorSchemes.tol_light.colors
palette = ColorSchemes.mk_12.colors


fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)


function create_hist(x, y)

    bins = [minimum(y):0.3:maximum(y);]
    mean_x = mean(x, dims = 2)[:, 1, :, 1]

    h1 = fit(Histogram, y, bins)
    xmap1 = StatsBase.binindex.(Ref(h1), y)

    rad = zeros(size(bins)[1])
    rad_std = zeros(size(bins)[1])
    precip = zeros(size(bins)[1])
    precip_std = zeros(size(bins)[1])
    temp = zeros(size(bins)[1])
    temp_std = zeros(size(bins)[1])



    for i = 1:size(bins)[1]
        rad[i] = mean(mean_x[:, 1][xmap1.==i])
        rad_std[i] = std(mean_x[:, 1][xmap1.==i])
        precip[i] = mean(mean_x[:, 2][xmap1.==i])
        precip_std[i] = std(mean_x[:, 2][xmap1.==i])
        temp[i] = mean(mean_x[:, 3][xmap1.==i])
        temp_std[i] = std(mean_x[:, 3][xmap1.==i])
    end

    return bins, rad, precip, temp

end


function plot_clim_density(f, ax1, ax2, (bins, rad, precip, temp), y)

    d = density!(ax1, y, color = (p_base[3], 0.2))
    l1 = lines!(ax2, bins, rad, color = palette[12], linewidth = 2.0, label = "Radiation")
    l2 = lines!(
        ax2,
        bins,
        precip,
        color = palette[6],
        linewidth = 2.0,
        label = "Precipitation",
    )
    l3 =
        lines!(ax2, bins, temp, color = palette[10], linewidth = 2.0, label = "Temperature")
    hlines!(ax2, 0, color = :black, linestyle = :dash)
    vlines!(ax2, median(y), color = p_base[6], linestyle = :dashdot, label = "Median")
    vlines!(ax2, mean(y), color = p_base[4], linestyle = :dash, label = "Mean")
    vlines!(
        ax2,
        percentile(y, 90),
        color = p_base[2],
        linestyle = :dash,
        label = "90th percentile",
    )
    xlims!(ax1, minimum(y), maximum(y))
    xlims!(ax2, minimum(y), maximum(y))

    ylims!(ax1, 0, 2)
    ylims!(ax2, -0.3, 0.3)

    xlims!(ax1, 1.2, 6)
    xlims!(ax2, 1.2, 6)

    Legend(f[1, :], ax2, orientation = :horizontal, framevisible = false)
    hidespines!(ax1, :t)
    hidespines!(ax2, :t)
end

clim_beech = create_hist(x_beech, y_beech)
clim_pine = create_hist(x_pine, y_pine)
clim_spruce = create_hist(x_spruce, y_spruce)

f = Figure(resolution = (1400, 500))

left = 50
top = 10

ax1 = Axis(
    f[2, 1], 
    xgridvisible = false, 
    ygridvisible = false,
    ylabel = "Density",
    )

ax2 = Axis(
    f[2, 1],
    yaxisposition = :right,
    xgridvisible = false,
    ygridvisible = false,
    xlabel = "Biomass Loss [%] Beech",
)


ax3 = Axis(f[2, 2], xgridvisible = false, ygridvisible = false)
ax4 = Axis(
    f[2, 2],
    yaxisposition = :right,
    xgridvisible = false,
    ygridvisible = false,
    xlabel = "Biomass Loss [%] Pine",
)

ax5 = Axis(f[2, 3], xgridvisible = false, ygridvisible = false)
ax6 = Axis(
    f[2, 3],
    yaxisposition = :right,
    xgridvisible = false,
    ygridvisible = false,
    xlabel = "Biomass Loss [%] Spruce",
    ylabel = "Anomaly [-]"
)

plot_clim_density(f, ax1, ax2, clim_beech, y_beech)


plot_clim_density(f, ax3, ax4, clim_pine, y_pine)

plot_clim_density(f, ax5, ax6, clim_spruce, y_spruce)

Label(
        f[2, 1, TopLeft()],
        "a)",
        font = "TeX Gyre Heros Bold",
        fontsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )

Label(
    f[2, 2, TopLeft()],
    "b)",
    font = "TeX Gyre Heros Bold",
    fontsize = 22,
    padding = (0, left, top, 0),
    halign = :right,
)


Label(
        f[2, 3, TopLeft()],
        "c)",
        font = "TeX Gyre Heros Bold",
        fontsize = 22,
        padding = (0, left, top, 0),
        halign = :right,
    )
# Label(f[2,1, TopLeft()], "a)", font = "TeX Gyre Heros Bold", textsize = 22, padding = (0, left, top, 0),halign = :right)
# Label(f[2,2, TopLeft()], "b)", font = "TeX Gyre Heros Bold", textsize = 22, padding = (0, left, top, 0),halign = :right)
# Label(f[2,3, TopLeft()], "c)", font = "TeX Gyre Heros Bold", textsize = 22, padding = (0, left, top, 0),halign = :right)
f
# Legend(f[1,1], [l1, l2, l3], ["Radiation", "Precipitation", "Temperature"], orientation = :horizontal)
save("/Users/anand/Documents/data/forest-mortality/images/density.pdf", f)
