using Luxor
using MathTeXEngine
using ColorSchemes
using Colors

## Some color palette 

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 2:8])

palette = ColorSchemes.mk_12.colors


function create_clim((c_x, c_y), n, alpha)
    for i = 1:3
        if i == 1
            setcolor(sethue(palette[12])..., alpha)
        elseif i == 2
            setcolor(sethue(palette[6])..., alpha)
        elseif i == 3
            setcolor(sethue(palette[10])..., alpha)
        end
        rand_line = [Point(10 * x + c_x, c_y + 3 * randn() + i * (20)) for x = 1:n]
        prettypoly(collect(rand_line), action = :stroke)
        sethue("grey20")
    end
end

function draw_state((c_x, c_y), color, alpha, n)
    setcolor(sethue(color)..., alpha)
    for i = 1:n
        circle(Point(c_x, c_y - i * 8), 4, action = :fill)
    end
end


function mticks(n, pos; startnumber = -4, finishnumber = 1, nticks = 5)
    @layer begin
        translate(pos)
        ticklength = get_fontsize()
        print(ticklength)
        line(Point(0, 0), Point(0, ticklength), action = :stroke)
        k = rescale(n, 0, nticks - 1, startnumber, finishnumber)
        ticklength = get_fontsize() * 1.3
        text_str = "t$(round(k))"[1:3]

        if k == 0
            text_str = "t"
        elseif k > 0
            text_str = "t+$(round(k))"[1:3]
        end
        text(
            text_str,
            O + (0, ticklength + 10),
            halign = :center,
            valign = :middle,
            angle = -getrotation(),
        )
    end
end


@draw begin
    setline(2)
    fontsize(14)
    create_clim((-200, -50), 36, 1)
    # tickline(Point(-320, -150), Point(280, -150), finishnumber = 1, major = 4, minor = 0, startnumber = -4)
    tickline(
        Point(-320, -150),
        Point(280, -150),
        startnumber = -4,
        finishnumber = 1,
        major = 4,
        major_tick_function = mticks,
    )

    create_clim((-320, -50), 12, 0.2)
    create_clim((160, -50), 12, 0.2)
    for i = 1:6
        alpha_state = 0.2
        if i == 2
            alpha_state = 1
        end
        alpha_mort = 0.2
        if i == 5
            alpha_mort = 1
        end
        draw_state((-440 + i * 120, -50), palette[2], alpha_state, 5)
        draw_state((-440 + i * 120, 50), palette[5], alpha_mort, 1)

    end

end 800 400

palette
