using Luxor
using ColorSchemes
using Colors

## Some color palette 

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 2:8])

palette = ColorSchemes.mk_12.colors

function midpoint(p1, p2)
    Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2)
end

function rightmidpoint(b)
    p1 = b[3]
    p2 = b[4]
    midpoint(p1, p2)
end

function leftmidpoint(b)
    p1 = b[1]
    p2 = b[2]
    midpoint(p1, p2)
end

function bottommidpoint(b)
    p1 = b[1]
    p2 = b[4]
    midpoint(p1, p2)
end

function topmidpoint(b)
    p1 = b[2]
    p2 = b[3]
    midpoint(p1, p2)
end

function leftbottom(b)
    b[1]
end

function righttop(b)
    b[3]
end
function textbox(p, text, color)
    sethue(color)
    b = box(p, 120, 50, action = :fillpreserve)
    sethue("grey20")
    strokepath()
    sethue("black")
    textcentered(text, p)
    sethue("grey20")
    return b
end

function create_lines((c_x, c_y), nlines, npoints)
    for i = 1:nlines
        if i == 2
            sethue(palette[12])
        elseif i == 4
            sethue(palette[6])
        elseif i == 7
            sethue(palette[10])
        end
        rand_line = [Point(10 * x + c_x, c_y + 3 * randn() + i * (15)) for x = 1:npoints]
        prettypoly(collect(rand_line), action = :stroke)
        sethue("grey20")
    end

end

function create_clim((c_x, c_y))
    for i = 1:3
        if i == 1
            sethue(palette[12])
        elseif i == 2
            sethue(palette[6])
        elseif i == 3
            sethue(palette[10])
        end
        rand_line = [Point(10 * x + c_x, c_y + 3 * randn() + i * (20)) for x = 1:12]
        prettypoly(collect(rand_line), action = :stroke)
        sethue("grey20")

    end
end

# origin()

@drawsvg begin
    setline(2)
    fontsize(14)
    x_d = 150

    era5 = textbox(Point(-x_d, -200), "ERA5 Data", p_base[7])
    awe_gen = textbox(Point(-x_d, 0), "AWE-GEN", p_base[5])
    vae = textbox(Point(-x_d, 200), "VAE", p_base[5])
    calib_data = textbox(Point(x_d, -200), "Calibration Data", p_base[7])
    formind = textbox(Point(x_d, 0), "FORMIND", p_base[5])
    proto = textbox(Point(x_d, 200), "Prototypes", p_base[3])

    arrow(bottommidpoint(era5), topmidpoint(awe_gen))
    arrow(bottommidpoint(calib_data), topmidpoint(formind))
    arrow(rightmidpoint(awe_gen), leftmidpoint(formind))
    arrow(bottommidpoint(awe_gen), topmidpoint(vae))
    arrow(bottommidpoint(formind), topmidpoint(proto))
    arrow(rightmidpoint(vae), leftmidpoint(proto))
    setdash("dash")
    arrow(leftbottom(formind), righttop(vae))
    setdash("solid")
    create_lines((-210, -160), 7, 12)
    # create_lines((-250, 10), 3, 12)
    create_clim((-60, -40))
    create_clim((-210, 60))

end
