using NPZ
using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using KernelDensity

## Some color palette 

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 2:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.mk_12.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])

## Load the data

function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    bins = npzread(data_path * "bins_Xs_train_$(pft).npy")
    Xs = npzread(data_path * "Xs_train_$(pft).npy")

    return bins, Xs

end




bins_beech, xs_beech = read_data("beech")
bins_pine, xs_pine = read_data("pine")
bins_spruce, xs_spruce = read_data("spruce")

xlabel = [
    L"\mathrm{Age} [Yr]",
    L"\mathrm{Stem \ V Volume} [m^{3}]",
    L"\mathrm{LAI} [-]",
    L"\mathrm{Height} [m]",
    L"\mathrm{Diameter} [m]",
]
yticks = [
    [0, 10, 50, 150, 300],
    [0, 10, 50, 150, 300],
    [0, 5, 10, 20, 40, 100],
    [0, 10, 50, 100, 250],
    [0, 10, 50, 100, 250],
]
xlims_vector = [(-0.1, 300), (-0.1, 300), (-0.1, 100), (-0.1, 250), (-0.1, 250)]

fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)


f = Figure(resolution = (1400, 900))
ax_beech = [
    Axis(
        f[1, i],
        yscale = Makie.pseudolog10,
        yticks = yticks[i],
        xgridvisible = false,
        ygridvisible = false,
    ) for i = 1:5
]
ax_pine = [
    Axis(
        f[2, i],
        yscale = Makie.pseudolog10,
        yticks = yticks[i],
        xgridvisible = false,
        ygridvisible = false,
    ) for i = 1:5
]
ax_spruce = [
    Axis(
        f[3, i],
        xlabel = xlabel[i],
        yscale = Makie.pseudolog10,
        yticks = yticks[i],
        xgridvisible = false,
        ygridvisible = false,
    ) for i = 1:5
]

function plot_bp!(ax, xs, bins, xlims_vector)
    for i = 1:5
        for j = 1:10
            boxplot!(
                ax[i],
                zeros(size(xs)[1]) .+ bins[i, j],
                xs[:, j, i],
                width = bins[i, 7] - bins[i, 6],
                color = p_base[7],
                show_outliers = false,
            )
            hidespines!(ax[i], :r, :t)
            ylims!(ax[i], xlims_vector[i])
        end
    end
    f
end


plot_bp!(ax_beech, xs_beech, bins_beech, xlims_vector)
plot_bp!(ax_pine, xs_pine, bins_pine, xlims_vector)
plot_bp!(ax_spruce, xs_spruce, bins_spruce, xlims_vector)

ax_beech[1].ylabel = "Count [-] Beech"
ax_pine[1].ylabel = "Count [-] Pine"
ax_spruce[1].ylabel = "Count [-] Spruce"

f

save("/Users/anand/Documents/data/forest-mortality/images/xs_plot.png", f, px_per_unit = 5)
