+using NPZ
using Makie
using CairoMakie
using StatsBase
using ColorSchemes
using Colors
using Dates
using KernelDensity

## Some color palette 

p_base = Dict(
    1 => "#29066B",
    2 => "#7D3AC1",
    3 => "#AF4BCE",
    4 => "#DB4CB2",
    5 => "#EB548C",
    6 => "#EA7369",
    7 => "#F0A58F",
    8 => "#FCEAE6",
)

cmap_base = ColorScheme([parse(Colorant, p_base[9-i]) for i = 2:8])

p_bg = Dict(1 => "#95D8EB", 2 => "#4DB4D7", 3 => "#0076BE", 4 => "#48BF91", 5 => "#8BD9C7")

palette = ColorSchemes.mk_12.colors

cmap_bg = ColorScheme([parse(Colorant, p_bg[i]) for i = 1:5])

## Load the data

function read_data(pft)
    data_path = "/Users/anand/Documents/data/forest-mortality/"

    nee = 100 * npzread(data_path * "nee_$(pft).npy")
    mbr = 100 * npzread(data_path * "mbr_$(pft).npy") # Convert to percent

    return nee, mbr

end

nee_beech, mbr_beech = read_data("beech")
nee_pine, mbr_pine = read_data("pine")
nee_spruce, mbr_spruce = read_data("spruce")

##### Do some plotting

fontsize_theme = Theme(fontsize = 18)
set_theme!(fontsize_theme)


f = Figure(resolution = (1400, 400))
ax_beech = Axis(
    f[2, 1],
    xgridvisible = false,
    ygridvisible = false,
    xlabel = "Mortality [%] Beech",
    ylabel = L" \mathrm{NEE}  \ a [gC \ mm^{-2}yr^{-1}]",
)
ax_pine =
    Axis(f[2, 2], xgridvisible = false, ygridvisible = false, xlabel = "Mortality [%] Pine")
ax_spruce = Axis(
    f[2, 3],
    xgridvisible = false,
    ygridvisible = false,
    xlabel = "Mortality [%] Spruce",
)

function plot_mbr_nee(f, ax, mbr, nee)

    # k = kde(hcat(mbr, nee))
    # contourf!(ax, k.x, k.y, k.density, levels = 100, colormap = (:Greens_8))

    scatter!(ax, mbr, nee, markersize = 3.0, color = (p_base[7], 0.1))



    vlines!(ax, median(mbr), color = p_base[5], linestyle = "-.", label = "Median")
    vlines!(ax, mean(mbr), color = p_base[3], linestyle = "--", label = "Mean")
    hlines!(ax, mean(nee), color = p_base[3], linestyle = "--")

    vlines!(
        ax,
        percentile(mbr, 90),
        color = p_base[1],
        linestyle = "--",
        label = "90th percentile",
    )

    xlims!(ax, 0, 15)
    ylims!(ax, -150, 200)

    hidespines!(ax, :r, :t)
    Legend(f[1, :], ax, orientation = :horizontal, framevisible = false)

end

plot_mbr_nee(f, ax_beech, mbr_beech, nee_beech)
plot_mbr_nee(f, ax_pine, mbr_pine, nee_pine)
plot_mbr_nee(f, ax_spruce, mbr_spruce, nee_spruce)

f

save("/Users/anand/Documents/data/forest-mortality/images/nee_mbr.png", f, px_per_unit = 5)
